from commoncore.core_ebots import *

class ExemploTask(EbotsTaskBase):
    
    task_name = 'NOME DA TAREFA CADASTRADA NO EBOTS' # <- Atualizar este valor com o nome da tarefa no EBots.

    def execute(self, business_key:str) -> None:

        """
        ####################################################################################################
        ##########                TEMPLATE PARA PROJETOS DE RPA UTILIZANDO EBOTS                  ##########
        ####################################################################################################

        ### NÃO SE ESQUEÇA DE REMOVER ESTES COMENTÁRIOS ANTES DE PUBLICAR SEU ROBÔ ###

        Utilize este exemplo como um modelo para classes de tarefas.

        Este exemplo está usando o modo 'AutoPilot' do EBots. Desta forma, o EBots vai automaticamente
        identificar esta classe, e executar o método 'execute' quando necessário. Este método será executado
        quando a tarefa com o nome indicado acima ser executada.

        ### business_key ###

        Em cada execução é recebido um valor de 'business_key'. Tarefas agendadas vão receber um business_key 
        vazio, já tarefas de itens vão receber o valor cadastrado no 'business_key' quando adicionadas.

        ### Obtendo parâmetros ###
        
        Caso precise do valor de um parâmetro cadastrado no EBots, utilize um dos exemplos abaixo:
            param = self.get_param('nome do parametro')
            credential = self.get_password('nome da senha')
            username = credential['username']
            password = credential['password']

        ### Disparando erros de negócio ###

        Um erro de negócio é um erro geralmente do dado fornecido. Não é um erro do robô ou do legado sendo
        utilizado. Todos os erros de negócio devem ser previstos, como por exemplo um cliente não existe,
        um produto não existe, ou uma determinada opção não estava disponível. 
        
        IMPORTANTE: Todos os erros de negócio devem ser previamente cadastrados no EBots.

        Para registrar um erro de negócio, utilize um dos exemplos abaixo:
            raise BusinessError('Cliente não encontrado')        
            raise BusinessError('Saldo insuficiente', f'Saldo: {saldo}')

        Observações:
        - Não é necessário incluir nenhuma informação do business_key no erro de negócio, pois este é 
          identificado automaticamente e já é exibido no Grafana.
        - O segundo parâmetro do BusinessError é uma mensagem opcional que será exibida no Grafana.
        - Tenha em mente que, após disparado o erro, nada mais será executado neste método.

        ### Disparando erros de sistema/robóticos ###

        O EBots automaticamente captura qualquer erro disparado durante a execução deste método e registra,
        incluindo log. Desta forma, é completamente desnecessário escrever seu método todo dentro de um try.
        
        Caso queira, você também pode disparar seus próprios erros com 'raise Exception'. Todos serão
        capturados, logados e a execução marcada como 'Falha de sistema' no Grafana.

        ### Adicionando itens na fila do EBots ###

        Abaixo exemplos de como colocar os itens na fila. Todos os itens vão entrar na fila no status informado.
            lista_itens = []
            lista_itens.append({'business_key':'{ "sku": "123456789", "valor": "0,00" }'})
            lista_itens.append({'business_key':'{ "sku": "987654321", "valor": "0,00" }'})
            self.set_job(lista_itens, 'Aguardando processamento', False)

        O método set_job retorna o número de itens efetivamente incluídos. Isto é importante quando utilizamos
        'False' no parâmetro 'allow_duplicates', porque não necessariamente o número de itens enviados será
        o número de itens incluídos, pois alguns podem ser repetidos.

        ### Logs ###

        Não é necessário fazer nada para ter a sua execução registrada e exibida no Grafana. O EBots em modo
        AutoPilot registra tudo automaticamente. 
        
        Porém caso queira logar alguma informação para um eventual debug ou investigação, utilize o exemplo abaixo:
            self.set_log_info('Teste de mensagem de log.')

        IMPORTANTE: Esta informação NÃO é exibida no Grafana para o usuário. 

        ### Histórico ###

        Em caso de erros, o EBots vai automaticamente informar no Grafana que a execução não foi bem sucedida
        e exibirá as mensagens fornecidas. Porém, caso queira exibir alguma informação em caso de sucesso,
        utilize o exemplo abaixo:
            self.historic = 'Mensagem que será exibida no Grafana'

        Durante a execução, é possível obter a lista das últimas execuções (histórico) de um item com o método
        'self.get_historic'. Isto é interessante quando um item é executado por várias tarefas e uma tarefa
        pode ter acesso a alguma informação salva no histórico por uma tarefa anterior.

        ### Próximo status ###

        Caso tenha optado por utilizar vários status e tarefas em seu robô, você vai precisar mudar seu item
        de status para ser executado por outra tarefa.

        Para alterar o status de um item, utilize o exemplo abaixo:
            self.next_status = 'Aguardando validação no sistema X'

        IMPORTANTE: A alteração de status só ocorrerá após o término da execução deste método.

        """